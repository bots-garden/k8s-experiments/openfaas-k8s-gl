# GitLab Runner [TESTS]
# sudo apt-get update
# sudo apt-get install gitlab-runner
load '../config.rb'

BOX_IMAGE = "bento/ubuntu-16.04"

Vagrant.configure("2") do |config|
  config.vm.box = BOX_IMAGE

  ENV['LC_ALL']="en_US.UTF-8"

  config.vm.define "#{RUNNERS_NAME}" do |node|

    node.vm.hostname = "#{RUNNERS_NAME}"
    
    node.vm.network "public_network", ip:"#{RUNNERS_PUB_IP}", bridge: "en0: Wi-Fi (AirPort)"
    node.vm.network "private_network", ip: "#{RUNNERS_IP}"

    node.vm.provider "virtualbox" do |vb|
      vb.memory = 512
      vb.cpus = 2
      vb.name = "#{RUNNERS_NAME}"
    end

    node.vm.provision :shell, inline: <<-SHELL
      echo "👋 setup"

      apt-get install -y sshpass
      #apt-get install mc -y

      # ----- Install Docker -----
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
      apt-key fingerprint 0EBFCD88
      add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
      apt-get update
      apt-get install -y docker-ce
      # -----------------------  

      # Install GitLab Runner
      curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
      apt-get install -y gitlab-runner

      # Install NodeJS
      curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
      apt-get -y install nodejs

      #apt-get -y install jq

      # Add entries to hosts file:
      echo "" >> /etc/hosts
      echo '#{GITLAB_IP} #{GITLAB_DOMAIN}' >> /etc/hosts
      echo '#{REGISTRY_IP} #{REGISTRY_DOMAIN}' >> /etc/hosts
      echo '#{K8S_SERVER_IP} #{K8S_SERVER_DOMAIN}' >> /etc/hosts 
      echo "" >> /etc/hosts

      # Add unsecure registry
      echo "" >> /etc/docker/daemon.json
      echo '{' >> /etc/docker/daemon.json
      echo '  "insecure-registries" : ["#{REGISTRY}"]' >> /etc/docker/daemon.json
      echo '}' >> /etc/docker/daemon.json
      echo "" >> /etc/docker/daemon.json

      service docker restart

      curl -sSL https://cli.openfaas.com | sudo -E sh

      # Registering GitLab Runner
      gitlab-runner register --non-interactive \
        --url "#{CI_REGISTRATION_URL}" \
        --name "simple_runner" \
        --registration-token #{CI_REGISTRATION_TOKEN} \
        --tag-list "simple" \
        --executor shell


      # Registering GitLab Runner
      gitlab-runner register --non-interactive \
        --url "#{CI_REGISTRATION_URL}" \
        --name "openfaas_runner" \
        --registration-token #{CI_REGISTRATION_TOKEN} \
        --tag-list "#{OPENFAAS_RUNNER_TAG}" \
        --executor shell
      
      # Registering token
      #  gitlab-runner register --non-interactive \
      #  --url "#{CI_REGISTRATION_URL}" \
      #  --name "docker_runner" \
      #  --registration-token #{CI_REGISTRATION_TOKEN} \
      #  --tag-list "#{DOCKER_RUNNER_TAG}" \
      #  --executor docker \
      #  --docker-image "docker:stable" \
      #  --docker-privileged


    SHELL

    # if manual registration: do a sudo before

    node.vm.provision :shell, run: "always", inline: <<-SHELL
      # add $USER to docker group
      usermod -a -G docker $USER
      # allowing non-sudo use
      chmod 666 /var/run/docker.sock
    SHELL

  end

end