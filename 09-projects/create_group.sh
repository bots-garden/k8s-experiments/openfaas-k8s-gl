#!/bin/sh

source core.sh

# create a user with your handle (k33G) and admin rights + ssh key
token=$OF_GITLAB_USER_TOKEN
gitlab_url=$OF_GITLAB_URL
registry_domain=$OF_DOCKER_REGISTRY_DOMAIN
openfaas_url=$OF_OPENFAAS_URL
# create a bot user on GitLab and admin rights
bot_token=$OF_GITLAB_BOT_TOKEN

group=$( rawurlencode "$1" )

echo "🚀 > creating $group"

# TODO Encode
# "/", "%2F"
# ".", "%2E"
# "-", "%2D"
# "_", "%5F"
# ".", "%2E"

# Create group

generate_post_group_data() {
cat <<EOF
  {"name":"$group","path":"$group"}
EOF
}

curl --request POST --header "PRIVATE-TOKEN: $token" "$gitlab_url/api/v4/groups" \
--header "Content-Type: application/json" \
--data "$(generate_post_group_data)"

# Create group variables
curl --request POST --header "PRIVATE-TOKEN: $token" "$gitlab_url/api/v4/groups/$group/variables" \
--form "key=GL_URL" \
--form "value=$gitlab_url"

curl --request POST --header "PRIVATE-TOKEN: $token" "$gitlab_url/api/v4/groups/$group/variables" \
--form "key=GL_TOKEN" \
--form "value=$bot_token"

curl --request POST --header "PRIVATE-TOKEN: $token" "$gitlab_url/api/v4/groups/$group/variables" \
--form "key=OPENFAAS_URL" \
--form "value=$openfaas_url"

curl --request POST --header "PRIVATE-TOKEN: $token" "$gitlab_url/api/v4/groups/$group/variables" \
--form "key=REGISTRY_DOMAIN" \
--form "value=$registry_domain"

#curl --request POST --header "PRIVATE-TOKEN: $token" "$gitlab_url/api/v4/groups/$group/variables" \
#--form "key=REMOTE_CREATE" \
#--form "value="

#curl --request POST --header "PRIVATE-TOKEN: $token" "$gitlab_url/api/v4/groups/$group/variables" \
#--form "key=REMOTE_DESTROY" \
#--form "value="

#curl --request POST --header "PRIVATE-TOKEN: $token" "$gitlab_url/api/v4/groups/$group/variables" \
#--form "key=REMOTE_SUFFIX" \
#--form "value="