
## DEMO SETUP

```shell
export OF_DOCKER_REGISTRY_DOMAIN=registry.test:5000
export OF_GITLAB_URL=http://gitlab.test     
export OF_GITLAB_DOMAIN=gitlab.test     
export OF_OPENFAAS_URL=http://k8s.test:31112
export OF_GITLAB_USER_TOKEN=jsYEM4Ech13P--zxvaZG
export OF_GITLAB_BOT_TOKEN=R8AqBSJzyDGARKX7i9Ty
```

`cat ~/.ssh/id_rsa_gitlab.pub`

```
sudo pico ~/.ssh/config` (if problems) or ~/.ssh/known_hosts
# Local GitLab  Demo account(s)
Host gitlab.test
  HostName gitlab.test
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_rsa_gitlab
```



## One Function demo



```shell
./create_group.sh demo-sunday
./create_project.sh one-function demo-sunday


mkdir one-function
cd one-function
faas template pull https://gitlab.com/openfaas-experiments/static-vue-express-template

faas-cli new nice-web-app --lang static-vue-express --gateway k8s.test:31112 --prefix registry.test:5000

# change .gitignore
# comment the template line

git init
git remote add origin git@gitlab.test:demo-sunday/one-function.git
git add .
git commit -m "🎉 initial commit"
git push -u origin master
```



```
faas-cli build -f ./hello-world.yml
faas-cli push -f ./hello-world.yml
faas-cli deploy -f ./hello-world.yml
```