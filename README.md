# openfaas-k8s-gl

## Pre-requisites

- VirtualBox
- Vagrant
- Docker client (on the host, at leadt at the begining)
- kubectl

At the end we'll have 4 Virtual machines

- Docker Registry
- MicroK8S + OpenFaaS
- GitLab
- Runners

## First, we need a Docker Registry

```shell
cd 01-docker-registry
vagrant up
```

On the host side, update your `hosts` file:

```
172.16.245.160 registry.test # then we can reach the registry here: http://registry.test:5000
```

the registry will be visible on the private network (only on the host)

### ⚠️ Declare your registry as an unsecure registry

Add `registry.test:5000` as insecure registry on the Docker client (on the host).

### Test your docker registry

#### Browse your docker registry

- connect to the registry: `curl http://registry.test:5000/v2/_catalog` to browse the images in your docker registry
- you should get `{"repositories":[]}`

#### Push an image to your docker registry

```shell
docker pull node:latest
docker tag node:latest registry.test:5000/node
docker push registry.test:5000/node
```

- connect to the registry: `curl http://registry.test:5000/v2/_catalog` to browse the images in your docker registry
- you should get `{"repositories":["node"]}` 🎉
- `curl http://registry.test:5000/v2/node/tags/list` to the the versions for a specific image
- you should get `{"name":"node","tags":["latest"]}` 🎉


## Install MicroK8S

There is some manual commands (TODO: script all the setup)

### What is MicroK8S

> TODO 🚧 WIP

### Setup - first part

```shell
cd 02-k8s-openfaas
vagrant up
```

Then, we need to enable some add-ons

```shell
vagrant ssh
# Enable the DNS Addon which deploys kube-dns for us.
microk8s.enable dns

# Enable the Registry Addon which deploys a docker private registry and expose it on localhost:32000. 
# The storage addon will be enabled as part of this addon.
microk8s.enable registry

# You can also enable the dashboard
# And then expose the dashboard using kubectl proxy, 
microk8s.enable dashboard

microk8s.enable ingress
```

### We need to be able to use kubectl outside the VM

Always "inside" the VM:

```shell
microk8s.config
# then:
# copy content to the host in the directory project ./.kube/config
# change `server: http://10.0.2.15:8080` by `server: http://k8s.test:8080`
# then `cp -r .kube $HOME/` ⚠️ not in the VM, but on the host
cp -r .kube $HOME/
```

See the sample in `./.kube/config`

```yaml
apiVersion: v1
clusters:
- cluster:
    server: http://k8s.test:8080
  name: microk8s-cluster
contexts:
- context:
    cluster: microk8s-cluster
    user: admin
  name: microk8s
current-context: microk8s
kind: Config
preferences: {}
users:
- name: admin
  user:
    username: admin
```

👋 On the host side, update your `hosts` file, and add this entry:

```
172.16.245.138 k8s.test # then you can ping http://k8s.test:8080
```

### Bearer Token

```shell
# generate the bearer token (⚠️ what is it?)
# on the host side
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user-token | awk '{print $1}')
```

> example:
```
eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJrdWJlcm5ldGVzLWRhc2hib2FyZC10b2tlbi05dng5cSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6ImEzOWEyNzcyLTRlMDktMTFlOS05ODE5LTA4MDAyN2VlODdjNCIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDprdWJlLXN5c3RlbTprdWJlcm5ldGVzLWRhc2hib2FyZCJ9.Wg1UIT1I09B2UyUC68SNKsx9jkLPEawMGZml6DX6WtyzeMzB0gfQEmXLeloA7Xmp8c2l-YWYLRq2CJDm8Wsrlv4j9yW5VJ8Xq6anZgcAM6OJwwKlbkrEGEcmfr7D39yDdoy6P38Qt_yxmrg8s4ykxQnIDySE5ow1EJq4wifYCGsR_qGEFLN3SpZ3v622VEdv3s8hE5brFmwSkx0pt1RQu2aTxloALRtN8iB67zvdfMwrpM--ilBUmXycFOYQ5hEJCINBQ_xcckJ4tBpScvjDuqDFdD4jgZSb8EvrFDiTVTY4yicDzrq8htRPDnWoMUwfGw726EnOx7DvOmcGvmQySw
```

> You can use as an authenticate method when connecting to the K8S dashboard

### Access Dashboard

On the host side, type:

```shell
# ⚠️ on the host side
kubectl proxy --accept-hosts=.* --address=0.0.0.0 &
```

Then open this url: http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/

And use the token

### Some commands

If you want to test that your local `kubectl` can access to the remote K8S server, try these commands

```shell
kubectl get nodes
kubectl get all --namespace kube-system
```

### Various / Helm 💥 (Optional, not needed right now)

```shell
vagrant ssh
# install
sudo snap install helm --classic
# initialize
sudo helm init
# check if it works
helm ls
```

### Resources

- https://webcloudpower.com/use-kubernetics-locally-with-microk8s
- https://www.b2-4ac.com/kubernetes-in-seconds-microk8s/
- https://dev.to/chiefnoah/-kubernetes-for-the-modern-developer--5am8
- http://blog.gsagnard.fr/utiliser-microk8s-pour-vos-developpements-kubernetes-en-local-linux/
- https://resources.bilimedtech.com/distributed-computing/lab6.php


## Install OpenFaas on MicroK8s

### Initial setup


> 👋 ⚠️ on the host side

> TODO: check: what are these commands doing exactly?

```shell
git clone https://github.com/openfaas/faas-netes
cd faas-netes
kubectl apply -f ./namespaces.yml
kubectl apply -f ./yaml

kubectl get deployments --namespace openfaas
```

### Access to the UI

The UI will now be accessible on port 31112
I can use http://k8s.test:31112/ui/ because I set this in my `hosts` file: (if you remember)

#### Deploy a new function from the ui

- click on **"Deploy ney function"**
- choose **"ASCII Cows"** and click on **DEPLOY**
- wait some seconds and then invoke the function from the ui or with a browser, or with a curl: `http://k8s.test:31112/function/cows`

You should get something like that:

```
  _____          _________________
 /     \        /                 \
( 1+2=3 )      ( 1+SIN(2x)+COS(3x) )
 \__  _/        \_____  __________/
   ( (                ) )
    \\                //
      \    (__)      /
           (oo)
    /-------\/
   / |     ||
  *  ||----||
     ^^    ^^
    COWculator
```

### Create a function "for real"

#### faas-cli

First you need to install the faas-cli on your host (the we're going to use it as a remote cli)

##### OSX

```shell
brew install faas-cli
# or
brew upgrade faas-cli
```

##### Other operating system

> 🚧 WIP

#### New function

All will happend on the host side.

#### Create "first" function

See `03-functions` directory

```shell
mkdir amazing-functions
cd amazing-functions
faas-cli new --lang node --gateway k8s.test:31112 --prefix registry.test:5000 first
```

the `first.yml` deployment file should look like that:

```yaml
provider:
  name: faas
  gateway: http://k8s.test:31112
functions:
  first:
    lang: node
    handler: ./first
    image: registry.test:5000/first:latest
```

#### Build "first" function

- show the content of the directory
- show the content of the `first.yml` file
- show the JavaScript (TODO: explain)

```shell
ls

.
├── first
│   ├── handler.js
│   └── package.json
├── first.yml
└── template
```

> So, building...

```shell
# ⚠️ you need a (running) locally docker client 👋
faas-cli build -f ./first.yml
# ... ⏳ wait ...
# Successfully tagged registry.test:5000/first:latest
# Image: registry.test:5000/first:latest built.
```

#### Push the image of the function to the docker registry

```shell
faas-cli push -f ./first.yml
# ... ⏳ wait ...
# [0] < Pushing first [registry.test:5000/first:latest] done.
# [0] worker done.
```

Try:

```shell
curl http://registry.test:5000/v2/_catalog
```

You should get:

```shell
{"repositories":["first","node"]}
```

#### Deploy the function to OpenFaas

```shell
faas-cli deploy -f ./first.yml
#Deployed. 202 Accepted.
#URL: http://k8s.test:31112/function/first

```

## Install GitLab

Add this entry to the `hosts` file of the host:
```
172.16.245.122 gitlab.test
```

```shell
vagrant up
# and wait
```
### Users

When GitLab is started, create the `root` user and at least another user  (and give him the administrator rights)

#### SSH

You need to add your ssh key:

```shell
cat ~/.ssh/id_rsa_gitlab.pub 
```
> it's the name of **my** ssh key

In case of "problem"

```shell
sudo pico ~/.ssh/config  # or ~/.ssh/known_hosts`

# and

# Local Demo account
Host gitlab.test
   HostName gitlab.test
   PreferredAuthentications publickey
   IdentityFile ~/.ssh/id_rsa_gitlab
```


> if you need to update something in the configuration file:
> - `sudo pico /etc/gitlab/gitlab.rb`
> - `sudo gitlab-ctl reconfigure`


### Backup

The below command will backup data in `/var/opt/gitlab/backups` by default:


```shell
vagrant ssh
sudo gitlab-rake gitlab:backup:create STRATEGY=copy
sudo cp -a /var/opt/gitlab/backups/. /home/backup
```


### Updates

> Core edition:

```shell
vagrant ssh
sudo apt-get update && sudo apt-get install gitlab-ce
```

> Enterprise edition:

```shell
vagrant ssh
sudo apt-get update && sudo apt-get install gitlab-ee
```

if updates are **toooooo** long

```
vagrant ssh
sudo pico /etc/sysctl.conf

# and add
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1

# then run
sudo sysctl -p
```

## Runner(s)

```shell
sudo apt-get update
sudo apt-get install gitlab-runner
```

### Avoid the unknown host error

I had this error when running my pipeleine:
```
Cloning repository...
Cloning into '/builds/k33g/hello-java'...
fatal: unable to access 'http://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@gitlab-demo.test/k33g/hello-java.git/': Could not resolve host: gitlab.test
```

To fix it: (see see https://abstractprogramming.blogspot.fr/2016/12/setting-up-gitlab-runner-to-use-docker.html)

```shell
vagrant ssh
sudo pico /etc/gitlab-runner/config.toml
```

and add this line: `extra_hosts = ["gitlab.test:172.16.245.122"]`

So, your `config.toml` should look like this:

```toml
concurrent = 1
check_interval = 0

[[runners]]
  name = "runner_02"
  url = "http://gitlab-demo.test/"
  token = "62003735f9146099292e79a749797d"
  executor = "docker"
  [runners.docker]
    extra_hosts = ["gitlab.test:172.16.245.122"]
    tls_verify = false
    image = "docker:stable"
    privileged = true
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
  [runners.cache]
```