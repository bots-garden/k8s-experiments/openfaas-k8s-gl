# if one function per repository

export OPENFAAS_URL="http://k8s.test:31112"
export REGISTRY_DOMAIN="registry.test:5000"
export HANDLER="hi"
export LANGUAGE="python3"
export FUNCTION_NAME="hi"
export SUFFIX=""

declare -a deploy_lines=(
  "# Deployment file for GitLab CI"
  "provider:"
  "  name: faas"
  "  gateway: $OPENFAAS_URL"
  "functions:"
  "  $FUNCTION_NAME$SUFFIX:"
  "    lang: $LANGUAGE"
  "    handler: ./$HANDLER"
  "    image: $REGISTRY_DOMAIN/$FUNCTION_NAME$SUFFIX:latest"
)
for j in "${deploy_lines[@]}"
do
  echo "$j" 
done > deploy.hi.yml

faas-cli build -f deploy.hi.yml
faas-cli push -f deploy.hi.yml
faas-cli deploy -f deploy.hi.yml